package prompt

import scala.collection.mutable.HashMap

class Prompt {
  private val prompt = "Todo > "
  private val availableCommands = new HashMap[String, PromptCommand]()

  buildHelpCommand("help", "Print the help message")

  def addAvailableCommand(command: PromptCommand) =
    availableCommands += command.input -> command

  def getCommand() = {
    val cmd = readLine(prompt)
    commandLookup(cmd) match {
      case Some(c) => c.callback()
      case None => println("no command :/")
    }
  }

  private def commandLookup(cmd: String) =
    try {
      Some(availableCommands(cmd))
    } catch {
      case e: java.util.NoSuchElementException => None
    }

  private def buildHelpCommand(input: String, description: String) =
    addAvailableCommand(
      new PromptCommand(
        input,
        description,
        () => {
          println("Here is a list of the available commands:")
          availableCommands.foreach {
            case (i, cmd) => println(cmd.input + " - " + cmd.description)
          }
        }
      )
    )
}
