package todo

class Todo(val body: String) {
  private var _done = false

  def done(): Unit = _done = true
  def undone(): Unit = _done = false
}
