package todo

import scala.collection.mutable.ArrayBuffer
import prompt.{Prompt, PromptCommand}

object Main extends App {
  TodoRunner().prompt()
}

case class TodoRunner() {
  private val todos = new ArrayBuffer[Todo]()
  def prompt(): Unit = {
    val prompt = new Prompt()
    prompt.getCommand()
  }
}
